# Association GUTenberg - Projet de statuts

Ce dépôt git contient :
- les statuts actuels de l'association, écrits en 1988 et révisés à plusieurs reprises jusqu'en 2005 (fichiers « GUTenberg-Statuts-1988.* »),
- la proposition de nouveaux statuts, dont l'adoption sera soumise au vote des adhérents lors de l'assemmblée générale du 12 novembre 2022 (fichiers « GUTenberg-Statuts.* »),
- une comparaison de la version de 2005 avec la proposition de 2022 (fichiers « GUTenberg_Statuts_comparaison_2005-2022.* »).
